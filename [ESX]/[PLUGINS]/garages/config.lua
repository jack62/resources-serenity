Config = {
	DrawDistance = 100,
	Price = 350,
	BlipInfos = {
		Sprite = 524,--369
		Color = 30,
		Seize = 0.8,
	}
}

Config.Fourriere = { x = 369.64, y = -1608.10, z = 28.35}

Config.Garages = {
	Garage_Centre = {	
		Id = "cubes",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x=236.12, y= -779.95, z= 29.6722},
			Heading = 160.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		}, 	
	},
	Garage_Nikola = {	
		Id = "nikola",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = 1269.06,y = -363.96,z = 68.08},
			Color = {r=0,g=200,b=200},
			Heading = 114.0,
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},		
	Garage_Paleto = {	
		Id = "paleto",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x=143.56, y= 6627.15, z= 30.7828},
			Heading = 160.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		}, 	
	},
	Garage_SandyShore = {	
		Id = "sandyshore",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x=1566.30, y= 3791.19, z= 33.25},
			Heading = 160.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		}, 	
	},
	Garage_GroveStreet = {	
		Id = "grovestreet",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -226.21,y = -1698.18,z = 32.95},
			Heading = 332.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 3.0},
			Marker = 36
		},	
	},	
	Garage_Ocean = {	
		Id = "ouest",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -2971.03,y = 360.95,z = 13.768},
			Heading = 160.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Hills = {	
		Id = "hills",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -813.43,y = 709.44,z = 146.05},
			Heading = 9.85,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		}, 	
	},	
	Garage_Plage = {	
		Id = "plage",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -1158.48,y = -1176.248,z = 4.63},
			Heading = 106.03,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		}, 	
	},
	Garage_Bahamamas = {	
		Id = "bahamas",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -1379.69,y = -475.51,z = 30.60},
			Heading = 94.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Mecano = {	
		Id = "mecano",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -356.66,y = -97.90,z = 44.66},
			Heading = 335.57,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},
	},
	Garage_Immo = {	
		Id = "immo",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -169.24,y = -630.27,z = 31.42},
			Heading = 200.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Docks = {
		Id = "docks",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = 1037.27,y = -3238.96,z = 4.90},
			Heading = 0.41,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		}, 	
	},
	Garage_Bank = {	
		Id = "banque",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = 156.13,y = 178.51,z = 104.39},
			Heading = 68.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Airline = {	
		Id = "airline",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -961.41,y = -2901.59,z = 12.96},
			Heading = 67.59,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Marina = {	
		Id = "marina",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -690.72,y = -1405.40,z = 4.02},
			Heading = 135.59,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Est = {	
		Id = "est",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = 2579.96,y = 438.31,z = 107.5},
			Heading = 2.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Chasse = {	
		Id = "chasse",
		Blip = true,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -1633.27,y = 4737.49,z = 52.26},
			Heading = 2.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Biker = {	
		Id = "biker",
		Blip = false,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = -221.21,y = -1698.18,z = 32.95},
			Heading = 270.0,
			Color = {r=0,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Crows = {	
		Id = "crows",
		Blip = false,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = 1001.75,y = -3172.260,z = -39.9},
			Heading = 1.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
	Garage_Slika = {	
		Id = "slika",
		Blip = false,
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		SpawnPoint = {
			Pos = {x = 1022.24,y = -3109.01,z = -39.9},
			Heading = 1.0,
			Color = {r=0,g=200,b=200},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Marker = 1
		},	
	},
}

--[[Config.GaragesMecano = {
	Bennys = {
		Marker = 1,
		SpawnPoint = {
			Pos = {x = 477.729,y = -1888.856,z = 25.094},
			Heading = 303.0,
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Marker = 1
		},
		DeletePoint = {
			Pos = {x = 459.733,y = -1890.335,z = 24.776},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Marker = 1
		}, 	
	},	
	-- Bennys2 = {
		-- Marker = 1,
		-- SpawnPoint = {
			-- Pos = {x=-190.455, y= -1290.654, z= 30.295},
			-- Color = {r=0,g=255,b=0},
			-- Size  = {x = 3.0, y = 3.0, z = 1.0},
			-- Marker = 1
		-- },
		-- DeletePoint = {
			-- Pos = {x=-190.379, y=-1284.667, z=30.233},
			-- Color = {r=255,g=0,b=0},
			-- Size  = {x = 3.0, y = 3.0, z = 1.0},
			-- Marker = 1
		-- }, 	
	-- },
}]]